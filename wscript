#! /usr/bin/env python2

def options(opt):
    pass

def configure(cfg):
    pass

def build(bld):
    bld.shlib(
        source = '''
            src/rules.cc
            src/entry.cc
            src/api.cc
            src/interface.cc
            src/cell.cc
            src/map.cc
            src/game.cc
            src/tools.cc
            src/action-ack.cc
            src/action-charge.cc
            src/action-colonize.cc
            src/action-construct.cc
            src/action-discharge.cc
            src/action-move.cc
            src/action-transfer.cc
            src/dumper.cc
        ''',
        target = 'prologin2013',
        use = ['stechec2'],
        defines = ['MODULE_COLOR=ANSI_COL_PURPLE',
            'MODULE_NAME="prologin2013"'],
        lib = ['dl'],
    )

    for test in ['cell', 'map', 'fight', 'actions']:
        bld.program(
            features = 'gtest',
            source = 'src/tests/test-%s.cc' % test,
            target = 'prologin2013-test-%s' % test,
            use = ['prologin2013', 'stechec2-utils'],
            includes = ['.'],
            defines = ['MODULE_COLOR=ANSI_COL_PURPLE',
                'MODULE_NAME="prologin2013"'],
        )

    bld.install_files('${PREFIX}/share/stechec2/prologin2013', [
        'prologin2013.yml',
    ])

